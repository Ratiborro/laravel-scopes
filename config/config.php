<?php
return [
    'models' => [
        'base_namespace' => 'App\\Models'
    ],
    'stubs' => [
        'path' => base_path('stubs/laravel-scopes'),
        'default_type' => 'string',
        'type_map' => [
            'double' => 'float',
            'bigint' => 'int',
            'bigserial' => 'int',
            'mediumint' => 'int',
            'smallint' => 'int',
            'tinyint' => 'int',
            'serial' => 'int',
            'smallserial' => 'int',
            'datetime' => 'date',
            'time' => 'date',
            'timestamp' => 'date',
            'boolean' => 'boolean'
        ]
    ],
    'scopes' => [
        'nameGeneratorClass' => \Ratiborro\LaravelScopes\Helpers\ScopeNameGenerator::class
    ],
    'columns' => [
        'default_type' => 'string',
        'exclude_columns' => ['id', 'created_at', 'updated_at', 'deleted_at'],
        'exclude_model_columns' => [
            // \App\Models\User::class => ['two_factor_secret']
        ],
        'dictionary' => [
            // 'name' => 'named'
        ]
    ],
];
