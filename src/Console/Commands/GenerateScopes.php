<?php

namespace Ratiborro\LaravelScopes\Console\Commands;

use Illuminate\Console\Command;
use Ratiborro\LaravelScopes\Helpers\DB;
use Ratiborro\LaravelScopes\Helpers\ModelReflection;
use Ratiborro\LaravelScopes\Helpers\ScopeGenerator;
use Exception;

class GenerateScopes extends Command
{
    protected $signature = 'make:scopes {model}';
    protected $description = 'Generate column scopes for model';

    protected $startLine = 0;
    protected $classEndLine = 0;

    protected $scopes;
    protected $path;
    protected $columns;
    protected $model;

    protected $scopeGenerator;
    protected $modelReflection;

    public function __construct(ScopeGenerator $scopeGenerator, ModelReflection $modelReflection)
    {
        parent::__construct();
        $this->scopeGenerator = $scopeGenerator;
        $this->modelReflection = $modelReflection;
    }

    public function handle()
    {
        $this->model = $this->getQualifiedModelName($this->argument('model'));

        $this->modelReflection->setModel($this->model);

        if (!$this->modelReflection->checkModelExists()) {
            $this->error('The model does not exist. Make sure the model namespace is correct. Current namespace: ' . $this->model);
            return 1;
        }

        try {
            $modelScopes = $this->modelReflection->getModelScopes();
        } catch (Exception $exception) {
            $this->error('Fails to get model scopes. Make sure the model is the correct PHP class');
            return 2;
        }

        $this->calculateStartLine($modelScopes);


        try {
            $this->columns = DB::getTableColumns($this->modelReflection->getModelTable());
            $excludedColumns = $this->getExcludedColumns();
            $columns = array_map(static function ($column) {
                return $column['type'];
            }, array_diff_key($this->columns, array_flip($excludedColumns)));
        } catch (Exception $exception) {
            $this->error('Fails to get model table columns. Make sure the model table exists (migration is already done)');
            return 3;
        }

        $existingScopeNames = array_column($modelScopes, 'name');
        try {
            $scopes = $this->scopeGenerator
                ->setColumns($columns)
                ->buildScopes($existingScopeNames);
        } catch (Exception $exception) {
            $this->error('Fails to build scopes, sorry');
            return 4;
        }

        if (!count($scopes)) {
            $this->info('All scopes have already been generated!');
            return 0;
        }

        try {
            $isGenerated = $this->modelReflection->addContent(
                $this->startLine,
                $this->scopeGenerator->getScopesString()
            );
        } catch (Exception $exception) {
            $this->error('Fails to add scopes to model');
            return 5;
        }

        $isGenerated
            ? $this->info("Scopes successfully integrated to $this->model model!")
            : $this->error("Error on integrating scopes to $this->model model!");

        return 0;
    }

    protected function getQualifiedModelName(string $name): ?string
    {
        $namespace = $this->getModelsNamespace();
        $name = trim($name, '\\/');
        $name = str_replace('/', '\\', $name);
        return $namespace ? "$namespace\\$name" : $name;
    }

    protected function getModelsNamespace(): string
    {
        return config('laravel-scopes.models.base_namespace', 'App\\Models');
    }

    protected function getExcludedColumns(): array
    {
        $columnsConfig = config('laravel-scopes.columns', []);
        $excludedColumns = $columnsConfig['exclude_columns'] ?? [];
        $excludedModelColumns = $columnsConfig['exclude_model_columns'][$this->model] ?? [];
        return array_merge($excludedColumns, $excludedModelColumns);
    }

    protected function calculateStartLine(array $modelScopes): int
    {
        if (empty($modelScopes)) {
            $this->startLine = $this->modelReflection->getClassEndLine();
        } else {
            foreach ($modelScopes as $scope) {
                $line = $scope->getEndLine();
                if ($line > $this->startLine) {
                    $this->startLine = $line;
                }
            }
        }
        return $this->startLine;
    }
}
