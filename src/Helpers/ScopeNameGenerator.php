<?php

namespace Ratiborro\LaravelScopes\Helpers;

use Illuminate\Support\Str;

class ScopeNameGenerator
{
    protected $column;
    protected $name;
    protected $type;
    protected $config;

    public function __construct(string $column = null, string $type = null)
    {
        $this->column = $column;
        $this->name = $column;
        $this->type = $type;
        $this->config = config('laravel-scopes.columns');
    }

    public function getColumn(): string
    {
        return $this->column;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getScopeName(): string
    {
        $name = $this->before($this->column);
        $method = $this->getMethodName();
        $name = $this->$method($name);
        $this->name = $this->after($name);
        return $this->name;
    }

    protected function getMethodName(string $type = null): string
    {
        $type = Str::studly($type ?: $this->type);
        $method = "get{$type}ScopeName";
        if (method_exists($this, $method)) {
            return $method;
        }
        $defaultType = $this->getDefaultType();
        if ($type === $defaultType) {
            return 'getStringScopeName';
        }
        $type = $defaultType;
        return call_user_func(__METHOD__, $type);
    }

    protected function getDefaultType(): string
    {
        return $this->config['default_type'] ?? 'string';
    }

    protected function dictionary(string $name): string
    {
        return $this->config['dictionary'][$name] ?? $name;
    }

    protected function before(string $name): string
    {
        return $this->dictionary($name);
    }

    protected function after(string $name): string
    {
        return 'scope' . Str::studly($name);
    }

    protected function getStringScopeName(string $name): string
    {
        return $name;
    }

    protected function getBigintScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getIntScopeName(string $name): string
    {
        if (Str::endsWith($name, '_id')) {
            return 'of_' . Str::beforeLast($name, '_id');
        }
        return $name;
    }

    protected function getMediumintScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getSmallintScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getTinyintScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getFloatScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getDoubleScopeName(string $name): string
    {
        return $this->getIntScopeName($name);
    }

    protected function getBooleanScopeName(string $name): string
    {
        if (Str::startsWith($name, 'is_')) {
            return Str::after($name, 'is_');
        }
        return $name;
    }

    protected function getDateScopeName(string $name): string
    {
        return $this->getDatetimeScopeName($name);
    }

    protected function getDatetimeScopeName(string $name): string
    {
        return $this->getStringScopeName($name);
    }

    protected function getTimeScopeName(string $name): string
    {
        return $this->getDatetimeScopeName($name);
    }

    protected function getTimestampScopeName(string $name): string
    {
        return $this->getDatetimeScopeName($name);
    }

    protected function getEnumScopeName(string $name): string
    {
        return $this->getStringScopeName($name);
    }

    protected function getJsonScopeName(string $name): string
    {
        return $this->getStringScopeName($name);
    }

    protected function getJsonbScopeName(string $name): string
    {
        return $this->getJsonScopeName($name);
    }
}
