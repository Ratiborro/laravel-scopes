<?php

namespace Ratiborro\LaravelScopes\Helpers;

use Illuminate\Support\Str;
use ReflectionClass;
use ReflectionMethod;

class ModelReflection
{
    protected $model;
    protected $reflection;
    protected $path;
    protected $classEndLine;
    protected $methods;

    public function setModel(string $model): self
    {
        $this->model = $model;
        return $this;
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function getClassEndLine(): ?int
    {
        return $this->classEndLine;
    }

    public function checkModelExists(): bool
    {
        return class_exists($this->model);
    }

    public function getModelReflection(): ReflectionClass
    {
        return isset($this->reflection)
            ? $this->reflection
            : $this->reflection = new ReflectionClass($this->model);
    }

    public function getModelMethods($filter = ReflectionMethod::IS_PUBLIC): array
    {
        $reflection = $this->getModelReflection();
        $this->path = $reflection->getFileName();
        $this->classEndLine = $reflection->getEndLine() - 1;
        return $this->methods = $reflection->getMethods($filter);
    }

    public function getModelScopes(): array
    {
        if (!isset($this->emthods)) {
            $this->methods = $this->getModelMethods();
        }
        return array_values(
            array_filter($this->methods, function ($method) {
                return Str::startsWith($method->name, 'scope');
            })
        );
    }

    public function getModelTable(): string
    {
        return $this->model::make()->getTable();
    }

    public function addContent(int $startLine, string $content): bool
    {
        $lines = file($this->path);
        $startLineContent = $lines[$startLine] ?? null;
        $lines[$startLine] = $content . $startLineContent;
        return (bool)file_put_contents($this->path, implode('', $lines));
    }
}
