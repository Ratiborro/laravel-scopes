<?php


namespace Ratiborro\LaravelScopes\Helpers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;

class ScopeGenerator
{
    protected $modelPath;
    protected $startLine;
    protected $columns = [];
    protected $scopeNames = [];
    protected $scopes = [];

    public function setModelPath(string $path): self
    {
        $this->modelPath = $path;
        return $this;
    }

    public function setColumns(array $columns): self
    {
        $this->columns = $columns;
        return $this;
    }

    public function setStartLine(int $line): self
    {
        $this->startLine = $line;
        return $this;
    }

    public function getScopes(): array
    {
        return $this->scopes;
    }

    public function getScopesString(): string
    {
        return $this->stringifyScopes();
    }

    public function buildScopes(array $exclude): array
    {
        $this->generateScopeNames();
        $this->filterScopes($exclude);

        $stubs = $this->getStubs();
        $stubPaths = [];
        foreach ($stubs as $stub) {
            $stubPaths[$stub->getBasename('.stub')] = $stub->getRealPath();
        }

        $this->columns = array_filter($this->columns, function ($column) {
            return isset($this->scopeNames[$column]);
        }, ARRAY_FILTER_USE_KEY);

        uksort($this->columns, function ($a, $b) {
            return $this->scopeNames[$a] <=> $this->scopeNames[$b];
        });


        $this->scopes = [];
        foreach ($this->columns as $column => $type) {
            $type = $this->stubsTypeMapping($type);
            $path = $stubPaths[$type] ?? $stubPaths[config('laravel-scopes.stubs.default_type', 'string')];
            $stub = $this->readStub($path);
            $this->replaceColumn($stub, $column)
                ->replaceName($stub, Str::after($this->scopeNames[$column], 'scope'));

            $this->scopes[$column] = $stub;
        }

        return $this->scopes;
    }

    protected function generateScopeNames(): array
    {
        foreach ($this->columns as $column => $type) {
            $this->scopeNames[$column] = $this->generateScopeName($column, $type);
        }
        return array_filter($this->scopeNames);
    }

    protected function generateScopeName(string $column, string $type): string
    {
        $class = config(
            'laravel-scopes.scopes.nameGeneratorClass',
            \Ratiborro\LaravelScopes\Helpers\ScopeNameGenerator::class
        );
        return (new $class($column, $type))->getScopeName();
    }

    protected function filterScopes(array $exclude): array
    {
        $this->scopeNames = array_diff($this->scopeNames, $exclude);
        return $this->scopeNames;
    }

    protected function getStubs(): array
    {
        $path = config('laravel-scopes.stubs.path', base_path('stubs/laravel-scopes'));
        if (!is_dir($path)) {
            $path = dirname(__DIR__) . '/Console/Commands/stubs';
        }
        return File::files($path);
    }

    protected function stubsTypeMapping(string $type): string
    {
        $map = config('laravel-scopes.stubs.type_map', []);
        return $map[$type] ?? $type;
    }

    protected function readStub(string $path): string
    {
        return file_get_contents($path);
    }

    protected function replaceName(&$stub, string $name): self
    {
        $stub = str_replace('{{$name}}', $name, $stub);
        return $this;
    }

    protected function replaceColumn(&$stub, string $column): self
    {
        $stub = str_replace('{{$column}}', $column, $stub);
        return $this;
    }

    protected function stringifyScopes(): string
    {
        $content = '';
        foreach ($this->scopes as $scope) {
            $content .= PHP_EOL . $scope;
        }
        return $content;
    }
}
