<?php

namespace Ratiborro\LaravelScopes\Helpers;

use Illuminate\Support\Facades\Schema;

class DB
{
    public static function getTableColumns(string $table): array
    {
        $columns = Schema::getColumnListing($table);
        return collect($columns)->mapWithKeys(function ($column) use ($table) {
            $data = Schema::getConnection()->getDoctrineColumn($table, $column)->toArray();
            $data['type'] = $data['type']->getName();
            return [$column => $data];
        })->toArray();
    }
}
