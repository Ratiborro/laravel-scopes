<?php

namespace Ratiborro\LaravelScopes;

use Illuminate\Support\ServiceProvider;
use Ratiborro\LaravelScopes\Console\Commands\GenerateScopes;

class LaravelScopesServiceProvider extends ServiceProvider
{
    const PACKAGE_NAME = 'laravel-scopes';

    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../config/config.php' => config_path(static::PACKAGE_NAME . '.php'),
            ], static::PACKAGE_NAME . '-config');

            $this->publishes([
                __DIR__ . '/Console/Commands/stubs' => base_path('stubs/' . static::PACKAGE_NAME),
            ], static::PACKAGE_NAME . '-stubs');

            $this->commands([
                GenerateScopes::class
            ]);
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/config.php', static::PACKAGE_NAME);
    }

}
